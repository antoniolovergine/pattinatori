import React from 'react';
import Home from './homePage/home/home.jsx'
import Template from './homePage/template/template.jsx'
import { BrowserRouter, Route } from 'react-router-dom';
import Switch from 'react-bootstrap/esm/Switch';
import Login from './login/Login.jsx';
import Register from './register/register.jsx'
function App() {
  return (
    <BrowserRouter>
      <Template>
        <Switch>
          <Route exact path='/' component={Home}/>
          <Route exact path='/login' component={Login}/>
          <Route exact path='/register' component={Register}/>
        </Switch>
      </Template>
    </BrowserRouter>
  );
}

export default App;
