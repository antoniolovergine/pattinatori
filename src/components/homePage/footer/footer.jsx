import React, { Component } from 'react'

export class footer extends Component {
    render() {
        return (
            <footer class="bg-light py-5">
                <div class="container"><div class="small text-center text-muted">Copyright © 2020 - Pattinatori Di Bari</div></div>
            </footer>
        )
    }
}

export default footer
