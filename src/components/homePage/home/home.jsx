import React, { Component } from 'react'
import Header from '../header/header.jsx'
import '../../../css/styles.css'
export class home extends Component {
    render() {
        return (
            <div>
                <Header />
            <section className="page-section bg-primary" id="about">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-7 text-center">
                            <h2 className="text-white mt-0">Impara a pattinare con noi!</h2>
                            <hr className="divider light my-4" />
                            <p className="text-white mb-4">
                                Affina la tua abilità nel pattinaggio o impara a pattinare attraverso i nostri appuntamenti e gli stage che organizziamo.
                                Sarai in grado di pattinare in maniera corretta e esplorare la città in maniera diversa.
                            </p>
                        </div>
                    </div>
                </div>
            </section>

            <section className="page-section" id="services">
                <div className="container">
                    <h2 className="text-center mt-0">I Nostri appuntamenti</h2>
                    <hr className="divider my-2" />
                    <div className="row">
                        <div className="col-lg-6 col-md-6 text-center">
                            <div className="mt-5">
                                <i className="fas fa-4x fa-gem text-primary mb-4"></i>
                                <h3 className="h4 mb-2">Lunedì</h3>
                                <p className="text-muted mb-0">Appuntamento in pista per migliorare le proprie abilità nel pattinaggio</p>
                            </div>
                        </div>
                        <div className="col-lg-6 col-md-6 text-center">
                            <div className="mt-5">
                                <i className="fas fa-4x fa-laptop-code text-primary mb-4"></i>
                                <h3 className="h4 mb-2">Mercoledì</h3>
                                <p className="text-muted mb-0">Freeride serale in giro per la città, rigorosamente in pattini!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            
            <section className="page-section bg-red text-white">
                <div className="container text-center">
                    <h2 className="mb-6">Non sei registrato?</h2>
                    <a className="btn btn-light btn-xl" href="/register">Registrati</a>
                </div>
            </section>

            <section className="page-section" id="contact">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-8 text-center">
                            <h2 className="mt-0">Contattaci!</h2>
                            <hr className="divider my-4" />
                            <p className="text-muted mb-5">Vuoi iniziare a pattinare con noi? Vuoi avere più informazioni su corsi e uscite programmate?</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-4 ml-auto text-center mb-5 mb-lg-0">
                            <i className="fas fa-phone fa-3x mb-3 text-muted"></i>
                            <div>(+39) 328-7651191</div>
                        </div>
                        <div className="col-lg-4 mr-auto text-center">
                            <i className="fas fa-envelope fa-3x mb-3 text-muted"></i>

                            <a className="d-block" href="mailto:pattinatoridibari@gmail.com">pattinatoridibari@gmail.com</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        )
    }
}

export default home
