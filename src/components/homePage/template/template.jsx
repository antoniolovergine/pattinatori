import React, { Component } from 'react'

import Footer from '../footer/footer.jsx'


export class template extends Component {
    render() {
        return (
            <div>
                {this.props.children}
                <Footer />
            </div>
        )
    }
}

export default template
