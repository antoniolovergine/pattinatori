import React, { Component } from 'react'
import logo from '../../common/images/PattinatoriLogo.jpg'
import '../../css/login.css'

export class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            showSuccess:false,
            showError:false
        }
    }

    changeText = e => {
        let nam = e.target.name;
        let val = e.target.value;
        this.setState({ [nam]: val })
    }

    onSubmit = e => {
        e.preventDefault();
    }

    LoginSuccess = (dataResults) => {
        const cache = {} 
        cache["user"] = dataResults;
        this.setState({
            showSuccess: true,
            success: "Login effettuato con successo",
            showError: false
        })
        
        this.props.history.push({
            pathname: '/userPage/userPage',
            state: {
                detail: cache
            }
        }) 
    }

    LoginSuccessHead = (dataResults) => {
        const cache = {} 
        cache["user"] = dataResults;
        this.setState({
            showSuccess: true,
            success: "Login effettuato con successo",
            showError: false
        })
        
        this.props.history.push({
            pathname: '/directorsPage/directorsPage',
            state: {
                detail: cache
            }
        }) 
    }


    LoginError = () => {

        this.setState({
            showSuccess: false,
            success: "",
            showError: true
        })

    }
    login = (event) => {

        fetch(`http://localhost:3001/users?idNumber=${this.state.matricola}&password=${this.state.password}`).then((data) =>{
            data.json().then((resp) =>{
                if(resp.length > 0)
                {
                    fetch(`http://localhost:3001/capo_reparto?idNumber=${this.state.matricola}`).then((data) => {
                        data.json().then((resp2) => {
                            console.warn("capo", resp2)
                            if (resp2.length > 0) {
                                this.setState({head: true})
                                this.LoginSuccessHead(resp)
                            }
                            else {
                                this.setState({head: false})
                                this.LoginSuccess(resp)
                            }
                        })
                    })
                }
                else
                {
                    alert("Please cheack username e password")
                }
            })
        })

    }
    render() {
        return (
            <div className="formLogin">
                <form className="form-signin" onSubmit={this.onSubmit}>
                    <img className="mb-4" src={logo} alt="logo-pdb" />
                    <label htmlFor="inputEmail" className="sr-only">Email address</label>
                    <input type="text" name="email" className="form-control" onChange={this.changeText} value={this.state.email} placeholder="Email" required autoFocus />
                    <label htmlFor="inputPassword" className="sr-only">Password</label>
                    <input type="password" name="password" value={this.state.password} onChange={this.changeText} className="form-control" placeholder="Password" required />
                    <div className="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="remember-me" /> Remember me
                        </label>
                    </div>
                    <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this.login}>Login</button>
                </form>
            </div>
        )
    }
}

export default Login
