import React, { Component } from 'react'
import {
    BrowserRouter as Router,
    Link
  } from 'react-router-dom';
import logo from '../../common/images/RegisterLogo.jpg'
import '../../css/register.css'
export class register extends Component {

    constructor(props){
        super(props)

        this.state = {
            nome:"",
            cognome:"",
            password:"",
            email:""
        }

        //this.RegisterService = new RegisterService();
    }


    changeText = e => {
        let nam = e.target.name;
        let val = e.target.value;
        this.setState({ [nam]: val })
    }

    onSubmit = e => {
        e.preventDefault();

    }


    RegisterSuccess = (dataResults) => {

        this.props.history.push('/user/userHome')

    }

    RegisterError = (errorData) => {

    alert("Errore nella registrazione")

    }

    register = e => {
        this.RegisterService.register(this.state.nome, this.state.cognome, 
                                    this.state.email, this.state.password, this.state.idNumber,
                                        this.state.idDepartment, this.state.codiceFiscale, this.state.holidays, this.state.bankHours,
                                            this.LoginSuccess, this.LoginError)
    }



    render() {
        return (
            <div className="register">
                <div className="row">
                    <div className="col-md-3 register-left">
                        <h3>Benvenuto</h3>
                        <p>Registrati alla piattaforma per essere sempre aggiornato</p>
                        <Link to="/login">
                        <input type="submit" name="" href="" value="Login" /><br />
                        </Link>
                    </div>
                    <div className="col-md-9 register-right">
                        <div className="tab-content" id="myTabContent">
                            <div className="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                 <img src={logo} alt="Logo GVM" />
                                <div className="row register-form">
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input type="text" className="form-control" name="nome" onChange={this.changeText} placeholder="Nome" value={this.state.nome} />
                                        </div>
                                        <div className="form-group">
                                            <input type="text" className="form-control" name="cognome" onChange={this.changeText} placeholder="Cognome" value={this.state.cognome} />
                                        </div>
                                        <div className="form-group">
                                            <input type="password" className="form-control" name="password" onChange={this.changeText} placeholder="Password " value={this.state.password} />
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <input type="email" className="form-control" name="email" onChange={this.changeText} placeholder="Email " value={this.state.email} />
                                        </div>
                                        <input type="submit" className="btnRegister" value="Register" onClick={this.register}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default register
